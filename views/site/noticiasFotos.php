<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NoticiasFotos */
/* @var $form ActiveForm */
?>
<div class="site-noticiasFotos">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php 
        
        $noticias= app\models\Noticias::find()->all();
       
        $listData= yii\helpers\ArrayHelper::map($noticias,'codigo',function($model){
                return $model->codigo . " - " . $model->titulo;
        });

        echo $form->field($model, 'cod_noticias')->dropDownList(
            $listData,
            ['prompt'=>'Selecciona una noticia']
            );
    ?>
    
    <?php 
        
        $fotos= app\models\Fotos::find()->all();
       
        /*$listData= yii\helpers\ArrayHelper::map($fotos,'codigo', function($model){
                return Html::img("@web/imgs/" . $model->nombre, ["class"=>'img-thumbnail']);
        });*/
        
        $listData= yii\helpers\ArrayHelper::map($fotos,'codigo', function($model){
                return $model->codigo . " - " . $model->nombre;
        });
        echo $form->field($model, 'cod_fotos')->dropDownList(
            $listData,
            ['prompt'=>'Selecciona una foto']
            );
    ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-noticiasFotos -->
