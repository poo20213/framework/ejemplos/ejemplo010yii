<?php
use yii\helpers\Html;
use yii\bootstrap4\Carousel;
?>
<div class="row">
    <div class="col-lg-12">
        <h2><?= $model->titulo ?><br></h2>
        <div class="bg-warning rounded p-2">Fecha:</div>
        <div class="p-1"><?= $model->fecha ?></div>
        <div class="bg-warning rounded p-2">Contenido:</div>
        <div class="p-1 mb-5"><?= $model->texto ?></div>
        <div class="mb-5">
        <?php
            //sacar las fotos de esa noticia
            $fotos=$model->getCodFotos()->all();
            //en este array quiero colocar todas las fotos como img
            $elementos=[];
            
            foreach($fotos as $foto){
                $elementos[]=Html::img("@web/imgs/" . $foto->nombre);
            }
            
            //pregunto si esa noticia tiene alguna foto
            if(count($elementos)==0){
                //en caso de uqe no tenga fotos, coloco una imagen fija
                echo Html::img("@web/imgs/nada.jpg",[
                    "class"=>'mx-auto col-lg-12'
                ]);
            }else{
                //en caso de que tenga fotos coloca un carrusel con todas
                echo Carousel::widget([
                    'items'=>$elementos,
                ]);

            }
        ?>
        </div>
        <div class="clearfix mb-3">
        <?=  
           Html::a("Añadir comentario",
                ['site/nuevocomentario','codigo'=>$model->codigo],
                ['class'=>'btn btn-primary float-left col-lg-5']
                );
            
        ?>
        
        <?=  
           Html::a("Ver comentarios",
                ['site/vercomentario','codigo'=>$model->codigo],
                ['class'=>'btn btn-primary float-right col-lg-5']
                );
            
         ?>
        </div>
        <div class="clearfix mb-3">
        <?=  
           Html::a("Editar noticia",
                ['site/editarnoticia','codigo'=>$model->codigo],
                ['class'=>'btn btn-primary float-left col-lg-5']
                );
            
        ?>
        
        <?=  
           Html::a("Eliminar noticia",
                ['site/confirmareliminarnoticia','codigo'=>$model->codigo],
                ['class'=>'btn btn-primary float-right col-lg-5']
                );
            
        ?>
        </div>
    
</div>
</div>