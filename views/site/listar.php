<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap4\Carousel;
?>
<h1><?= $noticia->titulo ?></h1>
<div><?= $noticia->texto ?></div>
<?php
echo Carousel::widget([
        'items' => $elementos,
        'options'=>[
                'class'=>'mx-auto col-lg-6'
            ],
        'controls' => ['<i class="fas fa-arrow-left fa-3x"></i>','<i class="fas fa-arrow-right fa-3x"></i>']

    ]);
?>
<h2 class="border rounded bg-secondary p-3 text-white text-center mb-5">Comentarios de la noticia</h2>
<?php
echo GridView::widget([
   "dataProvider"=>$dataProvider,
   "layout"=>"{items}",
   "columns" => [
       "texto",
       "fecha",
       [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{editar} {eliminar}',
            'buttons' => [
                'editar' => function ($url,$model) {
                    return Html::a('<i class="fas fa-edit"></i>',
                        ['site/editarcomentario', "codigo"=>$model->codigo],
                        //['class'=>"btn btn-primary"]
                            );
                },
                'eliminar' => function ($url,$model) {                                        
                    return Html::a('<i class="fas fa-trash-alt"></i>',
                            ['site/eliminarcomentario',"codigo"=>$model->codigo],
                            //["class"=>'btn btn-danger'],
                            [
                                'data'=>[
                                    'confirm'=> '¿Seguro que deseas eliminar el cometario?',
                                    'method' => 'post',
                                ]
                            ]
                            );
                },
	        ],
        ],
   ]
]);
?>

