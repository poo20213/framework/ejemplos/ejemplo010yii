<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Noticias;
use app\models\Comentarios;
use yii\helpers\Html;
use app\models\Fotos;
use yii\web\UploadedFile;
use app\models\NoticiasFotos;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $consulta=Noticias::find();
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta,
        ]);
        return $this->render('index',[
            'dataProvider'=> $dataProvider,
        ]);
    }

    public function actionNuevocomentario($codigo) {
        $model = new Comentarios();

        if ($model->load(Yii::$app->request->post())) {
            $model->cod_noticia=$codigo;
            if ($model->save()) {
                return $this->redirect(["site/vercomentario","codigo"=>$codigo]);
            }
        }

        return $this->render('formulario', [
            'model' => $model,
        ]);
        
    }
    
    public function actionVercomentario($codigo) {
        $consulta= Comentarios::find()->where(["cod_noticia"=>$codigo]);
        $noticia= Noticias::findOne($codigo);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta,
        ]);
        
            // sacar las fotos de esa noticia
            $fotos=$noticia->getCodFotos()->all();
            
            // en este array quiero colocar las fotos como img
            $elementos=[];
            
            foreach ($fotos as $foto){
                $elementos[] = Html::img("@web/imgs/" . $foto->nombre);
            }
              
        return $this->render("listar",[
            'dataProvider' => $dataProvider,
            "noticia" => $noticia,
            "elementos" => $elementos,
        ]);
    }
    
    public function actionEditarcomentario($codigo) {
        
        /**
         * Cargar los datos del comentario desde la base de datos
         */
        $comentario= Comentarios::findOne($codigo);
        
        /**
         * Quiero saber si he pulsado al boton de editar desde el listado de comentarios
         * o al boton modificar del formulario de actualizacion del comentario
         */
        if ($this->request->isPost) {
            //aqui llego si he pusado el boton de actualizar en el formulario del comentario
            if ($comentario->load(Yii::$app->request->post())) { // cargo los datos del formulario en el modelo
                if ($comentario->save()) {
                    return $this->redirect(["site/vercomentario","codigo"=>$comentario->cod_noticia]);
                }
            }
        }
        
        //aqui solo llega si he pulsado el boton editar comentario en el listado de comentarios
        return $this->render('formulario', [
            'model' => $comentario,
        ]);     
    }
    
    public function actionEliminarcomentario($codigo) {
        //Cargo el comentario a eliminar de la base de datos
        $comentario= Comentarios::findOne($codigo);
        
        // Obtengo la noticia del comentario a eliminar
        $cod_noticia =$comentario->cod_noticia;
        
        // Elimino el comentario
        $comentario->delete();
        
        //lllam  ala accion que nos muestra todosl los comentarios de la noticia ddonde he eliminado el comentario
        return $this->redirect(["site/vercomentario","codigo"=>$cod_noticia]);           
    }
    
    public function actionNuevanoticia() {
        $model = new Noticias(); // modelo para almacenar los datos

        // pregunto si vengo del formulario
        if ($model->load(Yii::$app->request->post())) { 
            if ($model->save()) { // intentando grabar los datos en la tabla
                return $this->redirect(["site/index"]); // cargo la tabla principal
            }
        }

        // crago el formulario porque
        // es la primera vez que llamo a la accion
        // o porque hay errores
        return $this->render('formularioNoticias', [
            'model' => $model,
        ]);
    }
    
    public function actionEditarnoticia($codigo) {
        /**
         * Cargar los datos del comentario desde la base de datos
         */
        $noticias= Noticias::findOne($codigo);
        
        /**
         * Quiero saber si he pulsado al boton de editar desde el listado de noticias
         * o al boton modificar del formulario de actualizacion de la noticia
         */
        if ($this->request->isPost) {
            //aqui llego si he pusado el boton de actualizar en el formulario de la noticia
            if ($noticias->load(Yii::$app->request->post())) { // cargo los datos del formulario en el modelo
                if ($noticias->save()) { // actualizo la noticia en la base de datos
                    return $this->redirect(["site/index"]);
                }
            }
        }
        
        //aqui solo llega si he pulsado el boton editar noticia en el listado de noticias
        return $this->render('formularioNoticias', [
            'model' => $noticias,
        ]);     
    }
    
    public function actionEliminarnoticia($codigo){
        //Cargo la noticia a eliminar de la base de datos
        $noticia= Noticias::findOne($codigo);

        // Elimino la noticia
        $noticia->delete();
        
        //lllam  ala accion que nos muestra todosl los comentarios de la noticia ddonde he eliminado el comentario
        return $this->redirect(["site/index"]);
    }
    
    public function actionConfirmareliminarnoticia($codigo){
        //Cargo la noticia a eliminar de la base de datos
        $noticia= Noticias::findOne($codigo);

        //lllam  ala accion que nos muestra todosl los comentarios de la noticia ddonde he eliminado el comentario
        return $this->render("ver",[
            "model" => $noticia,
        ]);
    }
    
    /*
     * Metodo para añadir imagenes a la base de datos
     * Añade la imagen a la carpeta web/imgs
     * y en la base de datos colocamos el nombre de la imagen
     */
    
    public function actionFotos() {
        $model =new Fotos();
        
        // comprobando si vengo del formulario
        if ($model->load(Yii::$app->request->post())) { 
            $model->nombre= UploadedFile::getInstance($model, 'nombre');
            if ($model->save()) { // intentando grabar los datos en la tabla
                return $this->redirect(["site/fotos"]); // cargo la tabla principal
            }
        }
        
        // cargo el formulario la primera vez o cuando hay errores
        return $this->render("formuFoto",[
            "model" => $model,
        ]);
    }
    
    public function actionListarfotos() {
        $activeDataProvider = new ActiveDataProvider([
            'query' => Fotos::find(),
        ]);
   
        return $this->render("listarfotos",[
            "dataProvider" => $activeDataProvider,
        ]);
    }
    
    
    public function actionNoticiasfotos()
    {
        $model = new NoticiasFotos();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {                
                return $this->redirect(['site/noticiasfotos']);
            }
        }

        return $this->render('noticiasFotos', [
            'model' => $model,
        ]);
    }
    
    public function actionEditarfoto($codigo) {
        
    }
    
    public function actionEliminarfoto($codigo) {
        $model = Fotos::findOne($codigo);
        
        $model->delete();
        return $this->redirect(["site/listarfotos"]);
        
    }
}
