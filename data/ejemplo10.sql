﻿DROP DATABASE IF EXISTS ejemplo10;
CREATE DATABASE ejemplo10;
USE ejemplo10;

CREATE TABLE noticias(
    codigo int AUTO_INCREMENT,
    titulo varchar (100),
    texto varchar (1000),
    fecha date,
    PRIMARY KEY(codigo)
);

CREATE TABLE fotos(
    codigo int AUTO_INCREMENT,
    nombre varchar (100),
    PRIMARY KEY (codigo)
);

CREATE TABLE comentarios(
    codigo int AUTO_INCREMENT,
    texto varchar (1000),
    fecha date,
    PRIMARY KEY (codigo),
    cod_noticia int NOT NULL,
    CONSTRAINT fkComentarios_noticias FOREIGN KEY (cod_noticia)
    REFERENCES noticias(codigo) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE noticias_fotos(
    cod_noticias int,
    cod_fotos int,
    visitas int DEFAULT 0,
    PRIMARY KEY(cod_noticias,cod_fotos),
    
    CONSTRAINT fkNoticiasFotos_noticias FOREIGN KEY (cod_noticias)
    REFERENCES noticias(codigo) ON DELETE CASCADE ON UPDATE CASCADE,

    CONSTRAINT fkNoticiasFotos_fotos FOREIGN KEY (cod_fotos)
    REFERENCES fotos(codigo) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO noticias (titulo, texto, fecha)
  VALUES ('Noticia 1', 'Ejemplo de noticia', '2021/11/09'),('Noticia 2', 'segunda noticia','2021/11/10');