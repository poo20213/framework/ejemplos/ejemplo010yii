<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comentarios".
 *
 * @property int $codigo
 * @property string|null $texto
 * @property string|null $fecha
 * @property int $cod_noticia
 *
 * @property Noticias $codNoticia
 */
class Comentarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comentarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['cod_noticia'], 'required'],
            [['cod_noticia'], 'integer'],
            [['texto'], 'string', 'max' => 1000],
            [['cod_noticia'], 'exist', 'skipOnError' => true, 'targetClass' => Noticias::className(), 'targetAttribute' => ['cod_noticia' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'texto' => 'Texto',
            'fecha' => 'Fecha',
            'cod_noticia' => 'Cod Noticia',
        ];
    }

    /**
     * Gets query for [[CodNoticia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodNoticia()
    {
        return $this->hasOne(Noticias::className(), ['codigo' => 'cod_noticia']);
    }
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        
        $this->fecha= date("Y/m/d");
        // este metodo me permite convertir un texto en una fecha valida para mi sitema
        //$this->fecha= \DateTime::createFromFormat("d/m/Y", $this->fecha)->format("Y/m/d");
        return true;
    }
    
    public function afterFind() {
        // voy a colocar la fecha en formato dia/mes/año cuando muestre registros
        // si la fecha no esta vacia
        parent::afterFind();
        if(!empty($this->fecha)){ 
            $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d/m/Y'); 
        }
    }
}
