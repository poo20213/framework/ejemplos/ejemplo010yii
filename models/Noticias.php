<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticias".
 *
 * @property int $codigo
 * @property string|null $titulo
 * @property string|null $texto
 * @property string|null $fecha
 *
 * @property Fotos[] $codFotos
 * @property Comentarios[] $comentarios
 * @property NoticiasFotos[] $noticiasFotos
 */
class Noticias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'date', 'format' => 'php:d/m/Y'],
            [['titulo'], 'string', 'max' => 100],
            [['texto'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'titulo' => 'Titulo',
            'texto' => 'Texto',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[CodFotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodFotos()
    {
        return $this->hasMany(Fotos::className(), ['codigo' => 'cod_fotos'])->viaTable('noticias_fotos', ['cod_noticias' => 'codigo']);
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasMany(Comentarios::className(), ['cod_noticia' => 'codigo']);
    }

    /**
     * Gets query for [[NoticiasFotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasFotos()
    {
        return $this->hasMany(NoticiasFotos::className(), ['cod_noticias' => 'codigo']);
    }
    
   /* public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
    }
     */   
    
    
    
    /*
     * este metodo se ejecuta automaticamente antes de almacenar o modificar una noticia
     */
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        
        // este metodo me permite convertir un texto en una fecha valida para mi sitema
        // en caso de que no este vacia
        if(!empty($this->fecha)){
            $this->fecha= \DateTime::createFromFormat("d/m/Y", $this->fecha)->format("Y/m/d");
        }
        return true;
    }
     
    
    // se ejecuta despues de realizar rl find ==> select * from noticias
    public function afterFind() {
        parent::afterFind();
        // voy a colocar la fecha en formato dia/mes/año cuando muestre registros
        // en caso de que no este vacia
        if(!empty($this->fecha)){
            $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d/m/Y'); 
        }
        
    }
    
}
