<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fotos".
 *
 * @property int $codigo
 * @property string|null $nombre
 *
 * @property Noticias[] $codNoticias
 * @property NoticiasFotos[] $noticiasFotos
 */
class Fotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fotos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'],'required'],
            [['nombre'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Foto',
        ];
    }

    /**
     * Gets query for [[CodNoticias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodNoticias()
    {
        return $this->hasMany(Noticias::className(), ['codigo' => 'cod_noticias'])->viaTable('noticias_fotos', ['cod_fotos' => 'codigo']);
    }

    /**
     * Gets query for [[NoticiasFotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasFotos()
    {
        return $this->hasMany(NoticiasFotos::className(), ['cod_fotos' => 'codigo']);
    }
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        
        // vamos a gestionar la subida de fotos
        if(!isset($this->nombre)){
            // en caso de que no la tenga que coja la antigua
            //$this->nombre= $this->getOldAttribute("nombre");
        }
        
        return true;
    }
    
    /**
     * este metyodo solamente se ejecuta despues de guardar el nombre de la foto
     * en la tabla
     * 
     */
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        // la funcion iconv la utilizo para que saveAs no me de problemas con las tildes y ñ
        $this->nombre->saveAs('imgs/' . $this->codigo . '_' . iconv('UTF-8','ISO-8859-1',$this->nombre->name), false);
        $this->nombre = $this->codigo . '_' . iconv('UTF-8','ISO-8859-1', $this->nombre->name);
        
        $this->updateAttributes(["nombre"]);
    }
        
    
}

