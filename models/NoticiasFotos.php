<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticias_fotos".
 *
 * @property int $cod_noticias
 * @property int $cod_fotos
 * @property int|null $visitas
 *
 * @property Fotos $codFotos
 * @property Noticias $codNoticias
 */
class NoticiasFotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias_fotos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_noticias', 'cod_fotos'], 'required'],
            [['cod_noticias', 'cod_fotos', 'visitas'], 'integer'],
            [['cod_noticias', 'cod_fotos'], 'unique', 'targetAttribute' => ['cod_noticias', 'cod_fotos']],
            [['cod_fotos'], 'exist', 'skipOnError' => true, 'targetClass' => Fotos::className(), 'targetAttribute' => ['cod_fotos' => 'codigo']],
            [['cod_noticias'], 'exist', 'skipOnError' => true, 'targetClass' => Noticias::className(), 'targetAttribute' => ['cod_noticias' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_noticias' => 'Cod Noticias',
            'cod_fotos' => 'Cod Fotos',
            'visitas' => 'Visitas',
        ];
    }

    /**
     * Gets query for [[CodFotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodFotos()
    {
        return $this->hasOne(Fotos::className(), ['codigo' => 'cod_fotos']);
    }

    /**
     * Gets query for [[CodNoticias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodNoticias()
    {
        return $this->hasOne(Noticias::className(), ['codigo' => 'cod_noticias']);
    }
}
